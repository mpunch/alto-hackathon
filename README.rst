ALTO Prototype data for DISA Hackathon
======================================

------------
Introduction
------------

The webpage of the ALTO project can be found here:

https://alto-gamma-ray-observatory.org/

The ALTO prototype detector is located  at the Linnaeus University 
campus between the D building and the Amfium.

It detects some of the particles that are produced in cascades by cosmic rays 
(protons and some higher nuclei) which interact in the upper atmosphere. 
The figure below is a rendering of cosmic rays showering particles onto Earth (Image by A. Chantelauze, S. Staffi, and L. Bret)

.. image:: images/CosmicRay.png
    :width: 320
    :alt: Cosmic Ray showering particles onto Earth

The ALTO prototype consists of 12 detector elements:

- two large "Cherenkov tanks" (CTs), which contain 25 tonnes of water with a very sensitive light detector at the bottom,
    - Cherenkov light is a blueish light given out by particles travelling faster than the speed of light in the medium they're going through,
- two "Scintillator tanks" (STs) which contain a smaller quantity of liquid which gives out "scintillation light" when fast particles pass through, with another sensitive light detector

- several smaller scintillator boxes or cans, for monitoring the passage of particles (direction, location) through the main tanks
    - "Surface Array" (SA) boxes: square boxes with :math:`\sim~$1\mathrm{m^2}` scintillator area
    - "Triggers" (Trigs): small solid scintillators, :math:`\sim~$400\mathrm{cm^2}` scintillator area
    - "Cans": small liquid scintillators in soft-drink cans, :math:`\sim~$40\mathrm{cm^2}` scintillator area

An image showing how these different detectors are laid out is shown here, looking from the North:

..  image:: images/schema_proto_feb21_2019.png
   :width: 800
   :alt: Schematic of the prototype detector layout


The numbers correspond to the order of the different detector elements read-out, 
where these correspond to detector elements described here, 
with the positions being the position of the light detector:

=======  =========  ===================== ====
Element  Name       Position (x,y,z) (m)  Description
=======  =========  ===================== ====
0        CT0        (-3, 0, 1.55)         Cherenkov tank 0 (East)
1        CT1        (3, 0, 1.55)          Cherenkov tank 1 (West)
4        ST0        (-3 , 0 , 0.2)        Scintillator tank 0 (East)
11       ST1        (3 , 0 , 0.2)         Scintillator tank 0 (West)
5        SA_TopCT0  (-4, -0.57735, 3.67)  Surface array box, on top of CT0 (East)
6        SA_Nth     (0, -1.73205, 0.12 )  Surface array box, on ground (North)
7        SA_Mid     (0, 0, 0.12 )         Surface array box, on ground (Middle)
10       SA_Sth     (0, 1.73205, 0.12)    Surface array box, on ground (South)
2        TrigBot    (1.65, 0.2, 0.05)     Trigger Bottom, below CT1 (West)
3        TrigTop    (1.35, 0, 3.65)       Trigger Top, above CT1 (West)
8        CanBot     (-2.82, -1.386, 0.09) Can Bottom, below ST0 (East)
9        CanTop     (-2.82, -1.386, 0.52) Scintillator tank 0 (West)
=======  =========  ===================== ====

The light detectors inside each detector give a negative 
charge-current "waveform" if they see a single photon, and these 
pile-up to make a bigger wave-form (like the traces on an 
oscilloscope) if there are several photons.  
If any two of the detectors sees a signal above a certain level (the "trigger 
threshold"), in a time-window of a few tens of nanoseconds, the 
electronics then reads out the wave-forms from all the detectors
simultaneously (even those that haven't seen anything), to give 
what is called an "Event". Each of the 12 wave-forms of an event 
consists of a series of 128 values corresponding to the charge in 
4 nanosecond time-bins.

The events can correspond to individual deeply penetrating 
particles from the cascade arriving at sea level ("muons", see first figure below), which 
would go in a straight downward-going line through two or more 
detectors, or to "shower particles" (electrons and gamma-rays) from the bottom of the cascade, which can go through several detectors simultaneously (see second figure).
For the Scintillator-type detectors, especially for those on the ground, there is a high rate of random signals from radioactivity.
These normally are not coincident with the shower signals, except by chance. 

.. image:: images/Muon.png
    :width: 320
    :alt: Example of a “muon” of giving signals in three ALTO detectors.

.. image:: images/Shower.png
    :width: 320
    :alt: Example of a “shower” of particles giving signals in many ALTO detectors.

Note: The signals should be more-or-less lined up in time, but 
because for instance of different lengths of cables connecting 
each of light detectors to the electronics box, there are fixed “
time-offsets” relative to an ideal time, which means that each of 
the detector elements then has a fixed “time-offset” with respect 
to any other. The ideal time is given by the time it takes for a 
muon to pass in the two detectors, considering that a muon 
travels at the speed of light.

--------
The data
--------

The data here consist of a reduced set of characteristics derived from the wave-forms.  For simplicity, we have removed any event where any one of the wave-forms couldn't be well-characterized, or where one of theme reached the saturation amplitude (maximum of the conversion electronics).

These are saved in a CSV (Comma Separated Variable) gzipped file.

The csv format is described below:

==========   ================    ===================================
Name         Type                Description
==========   ================    ===================================
event        integer             event number : as counted by readout code
Sec2018      unsigned integer    seconds since 1st January 2018, 00:00 UTC
Nanosecond   unsigned integer    nanoseconds in the current second
Q[]          float (array)       Integrated charge, proportional to area under wave-form. Integration window= [ToM-1.5*RiseTime, ToM+1.5*FallTime]
TOM[]        float (array)       Time of Maximum, in ns, interpolated with a parabola
TTH[]        float (array)       Time crossing Threshold of -100 mV, in ns (by linear interpolation)
A[]          short (array)       maximum amplitude of the signal, in tenths of mV --> A[i]/10. to be used
TOT[]        short (array)       Time over a lower Threshold of -10mV in samples (bins)
FT[]         short (array)       Fall Time (after the maximum, so is a rise) in samples
RT[]         short (array)       Rise Time (before the maximum, so is a fall) in samples
FWHM[]       short (array)       Full width at Half Maximum, in samples
==========   ================    ===================================

The characteristics are shown here annotated on a wave-form:

.. image:: images/Waveform_zoom_annotated.png
    :width: 800
    :alt: Annotated wave-form showing the derived characteristics
    
We get a rate of 800 events per second with this prototype, 
and here these are put into files which have about 10 million events 
(so about 40 minutes per file), for 400MB per gzipped CSV file.
    
An example Jupyter Python notebook for reading these is given here ("ALTO_ReadReducedCSV.ipynb"), which reads this into a Python Pandas structure (though with the fragile use of numpy arrays in the pandas cells, which may not work in later versions of pandas).  Examples of plotting the various characteristics are also given in that notebook.

---------------------------
Example Event Wave-forms
---------------------------

Showing some chosen events where there were only two elements in coincidence:

.. image:: images/Figure_1-0.png
    :width: 320
    :alt: Events with two elements

.. image:: images/Figure_1-1.png
    :width: 320
    :alt: Events with two elements

.. image:: images/Figure_1-2.png
    :width: 320
    :alt: Events with two elements

.. image:: images/Figure_1-3.png
    :width: 320
    :alt: Events with two elements

Showing some chosen events with several elements in coincidence:

.. image:: images/Figure_2-0.png
    :width: 320
    :alt: Events with two elements

.. image:: images/Figure_2-1.png
    :width: 320
    :alt: Events with two elements

.. image:: images/Figure_2-2.png
    :width: 320
    :alt: Events with two elements

.. image:: images/Figure_2-3.png
    :width: 320
    :alt: Events with two elements


-----
Tasks
-----

The hackathon participant should explore these data, for example:

- Search for clusters of event classes, e.g. corresponding to muons or showers, and look at the frequencies of these

  - Use the muon events to determine the fixed time-offsets between the detector elements. 

    - Hint: the muons passing through travel in straight lines, almost at the speed of light (speed of light in vacuum 2.99792x10^8 m/s)
    - Check if these fixed time-offsets are all coherent with each other

  - Use the determined fixed time-offsets to define a shorter coincidence time-window, to exclude signals in Scintillator triggers due to  radioactivity

- Look at the relations of the characteristics within the classes, and between the classes
  - e.g., for a muon-identified event where it goes through *CT0* and *SA_South*, is the distribution of Q very different to one going through *CT0* and *ST0* 

- Search for any other interesting things which could be found in these data ...

*Note*: Remember, if using the time of these particles to find speeds, time-offsets must be calculated beforehand.
